#define NUM_LEDS 60
#include "FastLED.h"
#define PIN 2
CRGB leds[NUM_LEDS];

int delayTime = 50;

int ledMode = 2;
int lastMode = 0;
byte counter = 0;
byte color = 0;
bool direction = 0;
byte mode = 0;
byte index = 0;
byte step = 0;
int len = 0;

// for bouncing balls
float Gravity = -9.81;
int StartHeight = 1;
const int BallCount = 3;

float Height[BallCount];
float ImpactVelocityStart = sqrt( -2 * Gravity * StartHeight );
float ImpactVelocity[BallCount];
float TimeSinceLastBounce[BallCount];
int   Position[BallCount];
long  ClockTimeSinceLastBounce[BallCount];
float Dampening[BallCount];

void setup() {
  FastLED.addLeds<WS2811, PIN, GRB>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
  FastLED.setBrightness(255);
  one_color_all(0, 0, 0);
  FastLED.show();
  Serial.begin(9600);

  for (int i = 0 ; i < BallCount ; i++) {
    ClockTimeSinceLastBounce[i] = millis();
    Height[i] = StartHeight;
    Position[i] = 0;
    ImpactVelocity[i] = ImpactVelocityStart;
    TimeSinceLastBounce[i] = 0;
    Dampening[i] = 0.90 - float(i) / pow(BallCount, 2);
  }
}

void loop() {
  if (Serial.available()) {  // если что то прислали
    delay(50);
    int newData = Serial.parseInt();  // парсим в тип данных int
    if (newData != 0) {
      lastMode = ledMode;
      ledMode = newData;
      if (ledMode != 30 && ledMode != 31 && ledMode != 999)
        one_color_all(0, 0, 0);
    }
  }

  Serial.print(ledMode);
  Serial.print(" ");
  switch(ledMode){
    case 999: break;
    case 1: one_color_all(0, 0, 0); break;
    case 2: raibowWave(); break;
    case 3: transfusion(); break;
    case 4: rainbowSlider(); break;
    case 5: randomizer(); break;
    case 6: runRed(); break;
    case 7: runRedTrain(); break;
    case 8: runRedBlue(); break;
    case 9: runRedBlueTrace(); break;
    case 10: flickerRed(); break;
    case 11: pulseToRedColor(); break;
    case 12: pulseColor(); break;
    case 13: wormtToCenter(); break;
    case 14: randomRed(); break;
    case 15: rainbowTrain(); break;
    case 16: russia(); break;
    case 17: runRedLoop(); break;
    case 18: white_temps(); break;
    case 19: sinusTrain(); break;
    case 20: doteToCenter(); break;
    case 21: bigSinus(); break;
    case 22: redToRainbow(); break; // не работает изза кривого рандома
    case 23: verticalRainbow(); break;
    case 24: randomPop(); break;
    case 25: policeLight(); break;
    case 26: rgb_sectors(); break;
    case 27: jumpFromCenter(); break;
    case 28: runRandomDote(); break;
    case 29: reversRainbowWave(); break;
    case 30: actual_to_end(); break;
    case 31: actual_to_start(); break;
    case 32: longWorm(); break;
    case 33: runWorm(); break;
    case 34: fier(); break;
    case 35: smallFastSinusTrain(); break;
    case 36: whiteFlashesOnWhite(); break;
    case 37: lotsOfRedDots(); break;
    case 38: lotsOfRainbowDots(); break;
    case 39: flickering(); break;
    case 40: bouncingBalls(); break; // снов лень, снова просто редачу
    default:
      Serial.print(ledMode);
      if (ledMode >= 256 && ledMode <= 511) {
        color = ledMode % 256;
      } else if (ledMode >= 512 && ledMode <= 768) {
        LEDS.setBrightness(ledMode % 256);
        ledMode = lastMode;
      }
  }
  FastLED.show();
  Serial.println();
  delay(delayTime);         // скорость движения радуги
}