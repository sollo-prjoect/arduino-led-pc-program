#include "COMProcess.h"
#include <cstdio>
#include <chrono>



char ReadCOM(HANDLE hSerial)
{
    DWORD iSize = 0;
    char sReceivedChar = '0';
    //size_t startTime = std::chrono::milliseconds();
    while(!iSize){
        //if(startTime - std::chrono::milliseconds < 1000)
        ReadFile(hSerial, &sReceivedChar, 1, &iSize, 0);
    }

    return  sReceivedChar;
}

DWORD WriteCom(HANDLE hSerial, const char* msg, int sizeMsg) {
    DWORD dwBytesWritten;
    BOOL iRet = WriteFile(hSerial, msg, sizeMsg, &dwBytesWritten, NULL);
    return iRet;
}

DWORD WriteCom(HANDLE hSerial, const int sendMode) {
    const int MAX_DIGITS = 10;
    char mode[MAX_DIGITS + sizeof(char)];
    sprintf_s(mode, "%d", sendMode);

    return WriteCom(hSerial, mode, strlen(mode));
}

HANDLE OpenCom(const char* newPort, bool startMode = false) {
    size_t newsize = strlen(newPort) + 1;
    wchar_t* port = new wchar_t[newsize];
    size_t convertedChars = 0;
    mbstowcs_s(&convertedChars, port, newsize, newPort, _TRUNCATE);

    LPCTSTR portName = port;
    HANDLE hSerial = ::CreateFile(portName, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
    if (hSerial == INVALID_HANDLE_VALUE)
    {
        if (GetLastError() == ERROR_FILE_NOT_FOUND)
        {
            throw 1;
        }
        throw 2;
    }
    DCB dcbSerialParams = { 0 };
    dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
    if (!GetCommState(hSerial, &dcbSerialParams))
    {
    }
    dcbSerialParams.BaudRate = CBR_9600;
    dcbSerialParams.ByteSize = 8;
    dcbSerialParams.StopBits = ONESTOPBIT;
    dcbSerialParams.Parity = NOPARITY;
    if (!SetCommState(hSerial, &dcbSerialParams))
    {
        throw 3;
    }
    return hSerial;
}
