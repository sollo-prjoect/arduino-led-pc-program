#pragma once
#include <Windows.h>

char ReadCOM(HANDLE hSerial);
DWORD WriteCom(HANDLE hSerial, const char* msg, int sizeMsg);
DWORD WriteCom(HANDLE hSerial, int sendMode);
HANDLE OpenCom(const char* newPort, bool startMode);
