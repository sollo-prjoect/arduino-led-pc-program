#include "mainwindow1.h"
#include "ui_mainwindow1.h"
#include "about.h"
#include "COMProcess.h"

#include "static.cpp"

#include <QSerialPortInfo>
#include <QMessageBox>
#include <QString>
#include <string.h>

MainWindow1::MainWindow1(QWidget *parent) :QMainWindow(parent), ui(new Ui::MainWindow1)
{
    //������ ���������� �����
    try{
        std::string strPortName = ReadData("last port.data").c_str();
        portName = new char[strPortName.length()+1];
        std::strcpy(portName, strPortName.c_str());
    }catch(int){
        portName = new char[5]{'C', 'O', 'M', '1', '\0'};
    }
    try{
        port = OpenCom(portName, false);
    }catch(int){
        QMessageBox::critical(this,"Error", "There are no connected devices to the standard port specified in the program, it is recommended to change the port to select the mode");
        //� ������������ �����, ���������� � ��������� ��� ������������ ���������, ��� ������ ������ ������������� �������� ����
    }
    ui->setupUi(this);

    connect(ui->pushButton,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_2,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_3,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_4,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_5,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_8,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_6,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_7,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_8,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_9,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_10,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_11,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_12,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_13,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_14,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_15,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_16,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_17,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_21,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_22,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_18,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_19,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_20,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_23,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_24,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_25,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_26,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_27,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_28,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_29,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_30,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_31,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_32,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_33,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_34,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_35,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_36,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_37,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_38,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_39,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_40,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_41,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_42,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_43,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_44,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_45,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->pushButton_46,SIGNAL(clicked()), this, SLOT(send_mode()));
    connect(ui->action, SIGNAL(triggered(bool)), this, SLOT(active_about()));
    connect(ui->comboBox, SIGNAL(activated(int)), this, SLOT(comboBox(int)));

    ui->comboBox->clear();
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos) {
        QStringList list;
        list << info.portName();
        ui->comboBox->addItem(list.first());
    }

    //������ ���������� ������
    try {
        lastMode = stoi(ReadData("last mode.data"));
    } catch (int) {
        lastMode = 0;
    }
    WriteCom(port, lastMode);
    start = true;
    pause = false;
}

MainWindow1::~MainWindow1()
{
    WriteData("last mode.data", lastMode);
    WriteData("last port.data", portName);


    delete portName;
    delete ui;
}

void MainWindow1::active_about()
{
    about window;
    window.setModal(true);
    window.exec();
}

void MainWindow1::send_mode(){
    staticData data;
    if(start && !pause){
        QPushButton* pb = (QPushButton*)sender();
        QString textButton = pb->text();
        for(int i = 0; i < 45; i++){
            if(textButton == data.buttonMode[i]){
                WriteCom(port, ++i);
                lastMode = i;
                //ReadCOM(port);
                break;
            }
        }
    }
}


void MainWindow1::comboBox(int index)
{
    try {
        CloseHandle(port);
    } catch (...) {}
    try { 
        std::string strPortName = ui->comboBox->currentText().toStdString().c_str();
        portName = new char[strPortName.length()+1];
        strcpy(portName, strPortName.c_str());
        port = OpenCom(portName, true);
    } catch (int) {
        QMessageBox::critical(this, "Error", "No device is connected to this port, please select another item from the list!");
        //� ������� ����� �� ���������� �� ���� ����������, ����������, �������� ������ ������� �� ������!
    }


    ui->comboBox->clear();
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos) {
        QStringList list;
        list << info.portName();
        ui->comboBox->addItem(list.first());
    }
    ui->comboBox->setCurrentIndex(index);

    WriteCom(port, lastMode);
}


void MainWindow1::on_pushButton_50_clicked()
{
    WriteCom(port, ++lastMode);
}


void MainWindow1::on_pushButton_49_clicked()
{
    WriteCom(port, --lastMode);
}

void MainWindow1::on_pushButton_47_clicked()
{
    if(!pause){
        ui->pushButton_47->setText("continue");
        WriteCom(port, 999);
    }else{
        ui->pushButton_47->setText("pause");
        WriteCom(port, lastMode);
    }
    pause=!pause;
}


void MainWindow1::on_pushButton_48_clicked()
{
    if(start){
        ui->pushButton_48->setText("on");
        WriteCom(port, 99);
    }else{
        ui->pushButton_48->setText("off");
        WriteCom(port, lastMode);
    }
    start=!start;
}

void MainWindow1::on_horizontalSlider_sliderReleased()
{
    if(start && !pause){
        lastMode = ui->horizontalSlider->sliderPosition()+256;
        WriteCom(port, ui->horizontalSlider->sliderPosition()+256);
    }
}


void MainWindow1::on_horizontalSlider_2_sliderReleased()
{
    if(start && !pause){
        WriteCom(port, ui->horizontalSlider_2->sliderPosition()+512);
    }
}

