#ifndef MAINWINDOW1_H
#define MAINWINDOW1_H

#include <QMainWindow>
#include <Windows.h>

#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

namespace Ui {
class MainWindow1;
}

class MainWindow1 : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow1(QWidget *parent = nullptr);
    ~MainWindow1();

private slots:
    void active_about();

    void send_mode();

    void comboBox(int index);

    void on_pushButton_50_clicked();

    void on_pushButton_49_clicked();

    void on_pushButton_47_clicked();

    void on_pushButton_48_clicked();

    void on_horizontalSlider_sliderReleased();

    void on_horizontalSlider_2_sliderReleased();

private:
    Ui::MainWindow1 *ui;
    HANDLE port;
    int lastMode;
    bool pause;
    bool start;
    char* portName;

};

#endif // MAINWINDOW1_H
