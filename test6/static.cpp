﻿#include <QString>
#include <fstream>
struct staticData
{
    QString buttonMode[46] = {"Плавная смена", "Линейный перелив", "Точечное заполненине",
                              "Ползающий огонёк", "Ползающий червячёк", "Бегущие точки",
                              "Красный/Синий","Красная эпилепсия", "Затузание/возникновение", "Белый/Красный",
                              "Червяки к центру", "Ебень", "Бегёщие огоньки", "Бегущий патриотизм",
                              "Частичное затухание/возникновение", "Бегущая точка", "Белый",
                              "Ползущие червки", "Ползущие огни", "Червь", "Зелёный-Ораньжевый", "Переливка к центру",
                              "Дёрганый салатовый", "Дискотека", "Мигалки", "Бегущий РГБ",
                              "Красный на батуте", "Бгущая стая", "Плавная переливка >", "Переливка",
                              "< Плавная перелика", "Бугёщий зелёный", "Прыгающий червь", "Огонь",
                              "2 червя", "Очень плавная переливка", "Дискатека 2", "Салатовые черви",
                              "Белаяя дискотека", "Белые мерцания", "Бегущие огни",
                              "Плавно бегущий РГБ", "Белая эпилепсия", "Красные шарики", "Прикол",
                              "Шары патриоты"};
};

int WriteData(const char* fileName, char* writeData){
    std::ofstream data;
    data.open(fileName);
    if (data.is_open()) data << writeData;
    else return 1;
    data.close();
    return 0;
}

int WriteData(const char* fileName, int value){
    const int MAX_DIGITS = 10;
    char mode[MAX_DIGITS + sizeof(char)];
    sprintf_s(mode, "%d", value);
    return WriteData(fileName, mode);
}

std::string ReadData(const char fileName[]){
    std::ifstream data;
    data.open(fileName);
    if (data.is_open()){
        std::string str = "";
        getline(data, str);
        return str;
    }else throw 1;
}

